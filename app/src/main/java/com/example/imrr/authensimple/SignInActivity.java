package com.example.imrr.authensimple;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.twitter.sdk.android.core.models.User;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.example.imrr.authensimple.R.id.userName;

/**
 * Created by imRR on 14-Nov-16.
 */


public class SignInActivity extends Activity{

    EditText signInUsername;
    EditText signInPassword;
    Button signInBtn;
    ProgressBar bar;

    AttemptLogin attemptLogin;
    String URL;
    String UserDisplay;
    String UserIdForLogin;
    String UserPass;
    JSONParser jsonParser = new JSONParser();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        initsInstance();

        signInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bar.setVisibility(View.VISIBLE);
                attemptLogin = new AttemptLogin();
                UserDisplay = signInUsername.getText().toString();
                URL = getResources().getString(R.string.url_for_login).toString();
                attemptLogin.execute(signInUsername.getText().toString(), signInPassword.getText().toString());
            }
        });
    }

    private void initsInstance() {
        signInUsername = (EditText) findViewById(R.id.signInUsername);
        signInPassword = (EditText) findViewById(R.id.signInPassword);
        signInBtn = (Button) findViewById(R.id.signInBtn);
        bar = (ProgressBar) findViewById(R.id.progressBarSignIn);
        bar.getIndeterminateDrawable().setColorFilter(
                getResources().getColor(R.color.black),
                android.graphics.PorterDuff.Mode.SRC_IN);

    }

    @Override
    public void onBackPressed(){
        Intent intent = new Intent(SignInActivity.this,
                MainActivity.class);
        startActivity(intent);
        finish();
    }

    class AttemptLogin extends AsyncTask {

        @Override
        protected Object doInBackground(Object[] params) {
            String userName = (String) params[0];
            String password = (String) params[1];

            ArrayList params2 = new ArrayList();
//            params2.add(new BasicNameValuePair("user id", userId));
            params2.add(new BasicNameValuePair("user name", userName));
            params2.add(new BasicNameValuePair("password", password));
//            System.out.println(params2.toString());
            JSONObject json = jsonParser.makeHttpRequest(URL, "POST", params2);


            return json;
        }

        protected void onPostExecute(Object result) {

            // dismiss the dialog once product deleted
            //Toast.makeText(getApplicationContext(),result,Toast.LENGTH_LONG).show();
            JSONObject JSONresult = (JSONObject) result;
            try {
                if (JSONresult != null) {
                    if(JSONresult.getInt("success")==1){
                        Intent intent = new Intent(SignInActivity.this,
                                InTimeActivity.class);
                        intent.putExtra("Username", UserDisplay);
                        intent.putExtra("currentUser", UserDisplay);
                        startActivity(intent);
                        finish();
                    }else
                    Toast.makeText(getApplicationContext(), JSONresult.getString("message"), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Unable to retrieve any data from server", Toast.LENGTH_SHORT).show();
                }
                bar.setVisibility(View.INVISIBLE);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        protected void onPreExecute() {
//            super.onPreExecute();

        }

    }
}


