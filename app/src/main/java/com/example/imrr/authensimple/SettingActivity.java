package com.example.imrr.authensimple;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.internal.CallbackManagerImpl;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import io.fabric.sdk.android.Fabric;

/**
 * Created by imRR on 14-Nov-16.
 */

public class SettingActivity extends FragmentActivity {

    Button signOutSelected;
    Button facebookSync;
    Button twitterSync;
    Button googleSync;
    SignInButton realGoogleBtnSync;
    AttemptLogin attemptLogin;
    GoogleApiClient mGoogleApiClient;
    LoginButton realFacebookBtnSync;
    CallbackManager callbackManager;
    TwitterLoginButton realTwitterBtnSync;

    Intent intent;
    String URL;
    String UserDisplay;
    String UserIdForLogin;
    String UserPass;
    String CurrentUser;
    boolean doubleBackToExitPressedOnce = false;
    JSONParser jsonParser = new JSONParser();
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    private final int GOOGLE_LOGIN_REQUEST_CODE = 1010;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        TwitterAuthConfig authConfig = new TwitterAuthConfig(
                getResources().getString(R.string.twitter_key), getResources().getString(R.string.twitter_secret));
        Fabric.with(this, new Twitter(authConfig));
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.activity_setting);

        initsInstance();


        realFacebookBtnSync.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
//                Toast.makeText(getApplicationContext(), "NOW FACEBOOK SUCCESS", Toast.LENGTH_SHORT).show();

                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.v("LoginActivity", response.toString());

                                try {
                                    UserDisplay = object.getString("name").toString();
                                    UserIdForLogin = object.getString("email").toString();
                                    UserPass = object.getString("id").toString();

                                    attemptLogin = new AttemptLogin();
                                    URL = getResources().getString(R.string.url_for_register).toString();
                                    attemptLogin.execute(object.getString("email").toString(), object.getString("id").toString());


//                                    goToLoginActivity();
                                    LoginManager.getInstance().logOut();


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday");
                request.setParameters(parameters);
                request.executeAsync();
//                UserDisplay = loginResult.getAccessToken();
//                parameters.getString();
            }

            @Override
            public void onCancel() {
                Toast.makeText(getApplicationContext(), "NOW FACEBOOK CANCEL", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(getApplicationContext(), "NOW FACEBOOK ON ERROR", Toast.LENGTH_SHORT).show();

            }
        });

        realGoogleBtnSync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                googleSignIn();

            }
        });

        realTwitterBtnSync.setCallback(new Callback<TwitterSession>() {

            @Override
            public void success(Result<TwitterSession> result) {
                UserDisplay = result.data.getUserName();
                UserIdForLogin = UserPass = Long.toString(result.data.getUserId());

                attemptLogin = new AttemptLogin();
                URL = getResources().getString(R.string.url_for_register).toString();
                attemptLogin.execute(UserIdForLogin, UserPass);

            }

            @Override
            public void failure(TwitterException exception) {
                Toast.makeText(getApplicationContext(), "twitter fail", Toast.LENGTH_SHORT).show();
            }
        });

        signOutSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.clear();
                editor.commit();
                Intent intent = new Intent(SettingActivity.this,
                        MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        googleSync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                googleSignIn();
            }
        });
    }

    private void initsInstance() {

        intent = getIntent();
        CurrentUser = intent.getStringExtra("currentUserId");
        URL  = getResources().getString(R.string.url_for_sync);

        signOutSelected = (Button) findViewById(R.id.signOutSelected);
        facebookSync = (Button) findViewById(R.id.facebookSync);
        googleSync = (Button) findViewById(R.id.googleSync);
        twitterSync = (Button) findViewById(R.id.twitterSync);

        realFacebookBtnSync = (LoginButton) findViewById(R.id.realFacebookBtnSync);
        realFacebookBtnSync.setReadPermissions(Arrays.asList(
                "public_profile", "email", "user_birthday", "user_friends"
        ));
        realTwitterBtnSync = (TwitterLoginButton) findViewById(R.id.realTwitterBtnSync);

        realGoogleBtnSync = (SignInButton) findViewById(R.id.realGoogleBtnSync);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
//        mGoogleApiClient = new GoogleApiClient.Builder(this)
//                .enableAutoManage(, null)
//                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
//                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(SettingActivity.this)
                .enableAutoManage(this, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        Toast.makeText(getApplicationContext(), "I don't know why connection fail.", Toast.LENGTH_SHORT).show();
                    }
                })
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
//                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
//                .addConnectionCallbacks(this).build();

        pref = getApplicationContext().getSharedPreferences("MyPref",0);
        editor = pref.edit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onBackPressed(){
        Intent intent = new Intent(SettingActivity.this,
                MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void googleSignIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, GOOGLE_LOGIN_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == GOOGLE_LOGIN_REQUEST_CODE) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
        //facebook
        if (requestCode == CallbackManagerImpl.RequestCodeOffset.Login.toRequestCode()) {
            callbackManager.onActivityResult(requestCode, resultCode, data);

        }
        //Twitter
        if(realTwitterBtnSync != null){
            realTwitterBtnSync.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void googleSignOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(@NonNull Status status) {

                    }
                }
        );
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {  // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount(); // get data from account.
            // acct.getDisplayName();
            onLoginComplete(true, acct);
        } else {  // Signed out, show unauthenticated UI.
            onLoginComplete(false, null);
        }
    }

    private void onLoginComplete(boolean success, GoogleSignInAccount acct) {
        if (success) {
//            String message = "\nID    = " + acct.getId()
//                    + "\nName  = " + acct.getDisplayName()
//                    + "\nEmail = " + acct.getEmail();
//            showDialog(message);

            UserDisplay = acct.getDisplayName().toString();
            UserIdForLogin  = acct.getEmail().toString();
            UserPass = acct.getId().toString();

            attemptLogin = new AttemptLogin();
            attemptLogin.execute(CurrentUser, "google_plus",UserPass);

            googleSignOut();

        } else {
//            showDialog("ERROR");
        }
    }

    private void showDialog(String msg) {
        AlertDialog.Builder builder =
                new AlertDialog.Builder(SettingActivity.this);
        builder.setMessage(msg);
        builder.show();
    }

    private class AttemptLogin extends AsyncTask {

        @Override
        protected Object doInBackground(Object[] params) {
            String user_name = (String) params[0];
            String social_type = (String) params[1];
            String social_id = (String) params[2];

            ArrayList params2 = new ArrayList();
//            params2.add(new BasicNameValuePair("user id", userId));
            params2.add(new BasicNameValuePair("user_name", user_name));
            params2.add(new BasicNameValuePair("social_type", social_type));
            params2.add(new BasicNameValuePair("social_id", social_id));
//            System.out.println(params2.toString());
            JSONObject json = jsonParser.makeHttpRequest(URL, "POST", params2);

            return json;
        }

        protected void onPostExecute(Object result) {

            // dismiss the dialog once product deleted
            //Toast.makeText(getApplicationContext(),result,Toast.LENGTH_LONG).show();
            JSONObject JSONresult = (JSONObject) result;
            try {
                if (JSONresult != null) {
                    Toast.makeText(getApplicationContext(), JSONresult.getString("message"), Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(getApplicationContext(), "Unable to retrieve any data from server", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        protected void onPreExecute() {
//            super.onPreExecute();

        }

    }

    private void goToLoginActivity() {
        Intent intent = new Intent(SettingActivity.this,
                InTimeActivity.class);
        intent.putExtra("Username", UserDisplay);
        startActivity(intent);
        finish();

    }
}
