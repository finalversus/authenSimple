package com.example.imrr.authensimple;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.SignInAccount;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.core.TwitterException;

import io.fabric.sdk.android.Fabric;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.internal.CallbackManagerImpl;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.core.models.User;
import com.twitter.sdk.android.core.services.AccountService;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.logging.Handler;
import java.util.logging.LogRecord;


public class MainActivity extends AppCompatActivity {

    EditText userId;
    EditText userName;
    EditText password;
    Button btnRegister;
    Button btnLogin;
    Button signInSelected;
    Button signUpSelected;
    Button facebookSelected;
    Button googleSelected;
    Button twitterSelected;
    TextView name;
    AttemptLogin attemptLogin;
    SignInButton realGoogleBtnLogin;
    GoogleApiClient mGoogleApiClient;
    LoginButton realFacebookBtnLogin;
    CallbackManager callbackManager;
    TwitterLoginButton realTwitterBtnLogin;
    TwitterAuthClient twitterAuthen;
    TwitterSession twtrSession;
    TwitterAuthToken twtrAuthToken;


//    ArrayList<HashMap<String, Object>> MyArrList = new ArrayList<HashMap<String, Object>>();

    String URL;
    String UserDisplay;
    String UserIdForLogin;
    String UserPass;
    String backgroundResult;
    String twtrToken;
    String twtrSecret;
    String checkInTime;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    boolean doubleBackToExitPressedOnce = false;
    ;
    boolean everCheckIn;
    Handler mHandler;
    JSONParser jsonParser = new JSONParser();
    JSONObject facebookData;
    int i = 0;
    boolean localLogin = true;
    private final int GOOGLE_LOGIN_REQUEST_CODE = 1010;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Login inits library
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        TwitterAuthConfig authConfig = new TwitterAuthConfig(
                getResources().getString(R.string.twitter_key), getResources().getString(R.string.twitter_secret));
        Fabric.with(this, new Twitter(authConfig));
        setContentView(R.layout.activity_home);
        callbackManager = CallbackManager.Factory.create();

        initsInstance();
        UserDisplay = pref.getString("currentUsername", null);
        UserIdForLogin = pref.getString("currentUserId", null);
        if (pref.getBoolean("everCheckIn", false)) {
            checkInTime = pref.getString("CheckInTime", null);
        }
        if (UserIdForLogin != null) {
            goToLoginActivity();
        }

        realFacebookBtnLogin.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
//                Toast.makeText(getApplicationContext(), "NOW FACEBOOK SUCCESS", Toast.LENGTH_SHORT).show();

                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.v("LoginActivity", response.toString());

                                try {
                                    UserDisplay = object.getString("name").toString();
                                    UserIdForLogin = object.getString("email").toString();
                                    UserPass = object.getString("id").toString();

                                    localLogin = false;
                                    attemptLogin = new AttemptLogin();
                                    URL = getResources().getString(R.string.url_for_register).toString();
                                    attemptLogin.execute(object.getString("email").toString(), object.getString("id").toString());


//                                    goToLoginActivity();
                                    LoginManager.getInstance().logOut();


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday");
                request.setParameters(parameters);
                request.executeAsync();
//                UserDisplay = loginResult.getAccessToken();
//                parameters.getString();
            }

            @Override
            public void onCancel() {
                Toast.makeText(getApplicationContext(), "NOW FACEBOOK CANCEL", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(getApplicationContext(), "NOW FACEBOOK ON ERROR", Toast.LENGTH_SHORT).show();

            }
        });

        realGoogleBtnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                googleSignIn();

            }
        });

        realTwitterBtnLogin.setCallback(new Callback<TwitterSession>() {

            @Override
            public void success(Result<TwitterSession> result) {
                UserDisplay = result.data.getUserName();
                UserIdForLogin = UserPass = Long.toString(result.data.getUserId());

                localLogin = false;
                attemptLogin = new AttemptLogin();
                URL = getResources().getString(R.string.url_for_register).toString();
                attemptLogin.execute(UserIdForLogin, UserPass);

            }

            @Override
            public void failure(TwitterException exception) {
                Toast.makeText(getApplicationContext(), "twitter fail", Toast.LENGTH_SHORT).show();
            }
        });

        signUpSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,
                        RegisterActivity.class);
                startActivity(intent);
                finish();
            }
        });
        signInSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,
                        SignInActivity.class);
                startActivity(intent);
                finish();
            }
        });
        googleSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                googleSignIn();
            }
        });
        facebookSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                realFacebookBtnLogin.performClick();
            }
        });
        twitterSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                realTwitterBtnLogin.performClick();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        // .... other stuff in my onResume ....
        this.doubleBackToExitPressedOnce = false;
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
    }

    private void initsInstance() {
        userName = (EditText) findViewById(R.id.userName);
        password = (EditText) findViewById(R.id.password);
        btnRegister = (Button) findViewById(R.id.btnRegister);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        name = (TextView) findViewById(R.id.name);
        realFacebookBtnLogin = (LoginButton) findViewById(R.id.realFacebookBtnLogin);
        realFacebookBtnLogin.setReadPermissions(Arrays.asList(
                "public_profile", "email", "user_birthday", "user_friends"
        ));
        realTwitterBtnLogin = (TwitterLoginButton) findViewById(R.id.realTwitterBtnLogin);

        realGoogleBtnLogin = (SignInButton) findViewById(R.id.realGoogleBtnLogin);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, null)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        //New instances :D
        signInSelected = (Button) findViewById(R.id.signInSelected);
        signUpSelected = (Button) findViewById(R.id.signUpSelected);
        facebookSelected = (Button) findViewById(R.id.facebookSelected);
        googleSelected = (Button) findViewById(R.id.googleSelected);
        twitterSelected = (Button) findViewById(R.id.twitterSelected);
        pref = getApplicationContext().getSharedPreferences("MyPref", 0);
        editor = pref.edit();


    }

    private void googleSignIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, GOOGLE_LOGIN_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == GOOGLE_LOGIN_REQUEST_CODE) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
        //facebook
        if (requestCode == CallbackManagerImpl.RequestCodeOffset.Login.toRequestCode()) {
            callbackManager.onActivityResult(requestCode, resultCode, data);

        }
        //Twitter
        if (realTwitterBtnLogin != null) {
            realTwitterBtnLogin.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void googleSignOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(@NonNull Status status) {

                    }
                }
        );
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {  // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount(); // get data from account.
            // acct.getDisplayName();
            onLoginComplete(true, acct);
        } else {  // Signed out, show unauthenticated UI.
            onLoginComplete(false, null);
        }
    }

    private void onLoginComplete(boolean success, GoogleSignInAccount acct) {
        if (success) {
//            String message = "\nID    = " + acct.getId()
//                    + "\nName  = " + acct.getDisplayName()
//                    + "\nEmail = " + acct.getEmail();
//            showDialog(message);

            UserDisplay = acct.getDisplayName().toString();
            UserIdForLogin = acct.getEmail().toString();
            UserPass = acct.getId().toString();

            localLogin = false;
            attemptLogin = new AttemptLogin();
//            URL = getResources().getString(R.string.url_for_register).toString();
            URL = getResources().getString(R.string.url_for_login).toString();
            attemptLogin.execute(acct.getEmail().toString(), acct.getId().toString());

            googleSignOut();

        } else {
//            showDialog("ERROR");
        }
    }

    private void showDialog(String msg) {
        AlertDialog.Builder builder =
                new AlertDialog.Builder(MainActivity.this);
        builder.setMessage(msg);
        builder.show();
    }

    private class AttemptLogin extends AsyncTask {

        @Override
        protected Object doInBackground(Object[] params) {
            String userName = (String) params[0];
            String password = (String) params[1];

            ArrayList params2 = new ArrayList();
//            params2.add(new BasicNameValuePair("user id", userId));
            params2.add(new BasicNameValuePair("user name", userName));
            params2.add(new BasicNameValuePair("password", password));
//            System.out.println(params2.toString());
            JSONObject json = jsonParser.makeHttpRequest(URL, "POST", params2);


            return json;
        }

        protected void onPostExecute(Object result) {

            // dismiss the dialog once product deleted
            //Toast.makeText(getApplicationContext(),result,Toast.LENGTH_LONG).show();
            JSONObject JSONresult = (JSONObject) result;
            try {
                if (JSONresult != null) {
                    if (localLogin) {
                        if (JSONresult.getInt("success") != 1) {
                            Toast.makeText(getApplicationContext(), JSONresult.getString("message"), Toast.LENGTH_SHORT).show();

                        }
                    } else {

                        if (JSONresult.getInt("success") == 0) {

                            attemptLogin = new AttemptLogin();
                            URL = getResources().getString(R.string.url_for_register).toString();
                            attemptLogin.execute(UserIdForLogin, UserPass);
                        } else if (JSONresult.getString("message").equals("Successfully registered the user")) {

                            attemptLogin = new AttemptLogin();
                            URL = getResources().getString(R.string.url_for_login).toString();
                            attemptLogin.execute(UserIdForLogin, UserPass);
                            localLogin = true;
                        }

                    }
                    if (JSONresult.getString("message").equals("Successfully logged in")) {
                        if (JSONresult.getString("username") != null) {
                            UserDisplay = UserIdForLogin = JSONresult.getString("username");
                        }
                        goToLoginActivity();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Unable to retrieve any data from server", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        protected void onPreExecute() {
//            super.onPreExecute();

        }

    }

    private void goToLoginActivity() {
        Intent intent = new Intent(MainActivity.this,
                InTimeActivity.class);
        intent.putExtra("Username", UserDisplay);
        intent.putExtra("currentUser", UserIdForLogin);
        intent.putExtra("checkInTime", checkInTime);
        startActivity(intent);
        finish();

    }
}
