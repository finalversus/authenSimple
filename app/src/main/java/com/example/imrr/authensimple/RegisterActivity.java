package com.example.imrr.authensimple;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by imRR on 14-Nov-16.
 */

public class RegisterActivity extends Activity{

    ProgressBar bar;
    EditText signUpUsername;
    EditText signUpPassword;
    EditText signUpConPassword;
    Button signUpBtn;

    AttemptLogin attemptLogin;
    String URL;
    String UserDisplay;
    String UserIdForLogin;
    String UserPass;
    boolean canSignUp = false;
    JSONParser jsonParser = new JSONParser();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initsInstance();

        signUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                canSignUp = checkCorrect();
                if(canSignUp){
                    bar.setVisibility(View.VISIBLE);
                    attemptLogin = new AttemptLogin();
                    URL = getResources().getString(R.string.url_for_register).toString();
                    attemptLogin.execute(signUpUsername.getText().toString(), signUpPassword.getText().toString());


                }else Toast.makeText(getApplicationContext(), "Invalid Username or Password", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initsInstance() {

        signUpUsername = (EditText) findViewById(R.id.signUpUsername);
        signUpPassword = (EditText) findViewById(R.id.signUpPassword);
        signUpConPassword = (EditText) findViewById(R.id.signUpConPassword);
        signUpBtn = (Button) findViewById(R.id.signUpBtn);
        bar = (ProgressBar) findViewById(R.id.progressBarSignUp);
        bar.getIndeterminateDrawable().setColorFilter(
                getResources().getColor(R.color.black),
                android.graphics.PorterDuff.Mode.SRC_IN);
    }

    @Override
    public void onBackPressed(){
        Intent intent = new Intent(RegisterActivity.this,
                MainActivity.class);
        startActivity(intent);
        finish();
    }

    private boolean checkCorrect() {
        String Password = signUpPassword.getText().toString();
        String ConPassword = signUpConPassword.getText().toString();
        if(signUpUsername.getText().toString().equals("")||Password.equals("") ||ConPassword.equals("")) {
            System.out.println("Empty ---------------------------------");
            return false;
        }
        if(!Password.equals(ConPassword)){
            System.out.println("Not Match ---------------------------------");
            return false;
        }
        return true;
    }

    class AttemptLogin extends AsyncTask {

        @Override
        protected Object doInBackground(Object[] params) {
            String userName = (String) params[0];
            String password = (String) params[1];

            ArrayList params2 = new ArrayList();
//            params2.add(new BasicNameValuePair("user id", userId));
            params2.add(new BasicNameValuePair("user name", userName));
            params2.add(new BasicNameValuePair("password", password));
//            System.out.println(params2.toString());
            JSONObject json = jsonParser.makeHttpRequest(URL, "POST", params2);


            return json;
        }

        protected void onPostExecute(Object result) {

            // dismiss the dialog once product deleted
            //Toast.makeText(getApplicationContext(),result,Toast.LENGTH_LONG).show();
            JSONObject JSONresult = (JSONObject) result;
            try {
                if (JSONresult != null) {
                    Intent intent = new Intent(RegisterActivity.this,
                            MainActivity.class);
                    startActivity(intent);
                    finish();
                    Toast.makeText(getApplicationContext(), JSONresult.getString("message"), Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(getApplicationContext(), "Unable to retrieve any data from server", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        protected void onPreExecute() {
//            super.onPreExecute();

        }

    }
}
