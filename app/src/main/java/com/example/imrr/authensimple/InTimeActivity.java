package com.example.imrr.authensimple;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

import okhttp3.internal.DiskLruCache;

/**
 * Created by imRR on 25-Oct-16.
 */

public class InTimeActivity extends Activity {

    Button btnLogout;
    TextView name;
    TextView inTimeUsername;
    TextView checkInTime;
    TextView checkOutTime;
    TextView differentTime;
    Button buttonCheckOut;
    Button buttonCheckIn;
    Button buttonSetting;
    ProgressBar bar;

    String currentTimeOutString;
    String currentTimeInString;
    String currentDateCheckIn;
    String currentDateCheckOut;
    String startedDateTime;
    String currentUserId;
    String currentUserName;
    String URL;
    int nowMinute;
    int nowHour;
    int nowSecond;
    final static double hourInMillis = 3600000.;
    double pointage;
    double workedTime;
    long nowCheckIn;
    long nowCheckOut;
    boolean waitngForCheckOut = true;
    android.text.format.DateFormat df;
    JSONParser jsonParser;
    AttemptLogin attemptLogin;
    //    SaveSharedPreference pref;
    SharedPreferences pref;
    SharedPreferences.Editor editor;

    GoogleApiClient mGoogleApiClient;
    private boolean doubleBackToExitPressedOnce = false;
    boolean localLogin = true;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_login);
        setContentView(R.layout.activity_intime);
        initsInstance();

        inTimeUsername.setText(currentUserName);
        editor.putString("currentUsername", currentUserName);
        editor.putString("currentUserId", currentUserId);
        editor.putBoolean("everCheckIn", false);
        editor.commit();

        if (pref.getString("CheckInTime", null) != null) {
            String showTime = pref.getString("CheckInTime", null);
            checkInTime.setText("Last check in time : " + showTime);
        }

        setCurrentTimeOut();
        Thread t = new Thread() {

            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (waitngForCheckOut)
                                    setCurrentTimeOut();

                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();

        buttonCheckIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bar.setVisibility(View.VISIBLE);
                setCurrentTimeIn();
                buttonCheckIn.setVisibility(View.GONE);
                buttonCheckOut.setVisibility(View.VISIBLE);
                waitngForCheckOut = true;
                setCurrentTimeOut();
                nowCheckIn = System.currentTimeMillis();

                attemptLogin = new AttemptLogin();
                URL = getResources().getString(R.string.url_for_check_in).toString();
                startedDateTime = currentDateCheckIn;
                attemptLogin.execute(currentUserId, startedDateTime, currentDateCheckOut, String.format("%.2f", pointage));

                editor.putBoolean("everCheckIn", true);
                editor.putString("CheckInTime", startedDateTime);
                editor.commit();
            }
        });

        buttonCheckOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bar.setVisibility(View.INVISIBLE);
                buttonCheckOut.setVisibility(View.GONE);
                buttonCheckIn.setVisibility(View.VISIBLE);
                waitngForCheckOut = false;
                nowCheckOut = System.currentTimeMillis();
                workedTime = (nowCheckOut - nowCheckIn) / hourInMillis;
                differentTime.setText(String.format("%.2f", workedTime));

                attemptLogin = new AttemptLogin();
                pointage = workedTime;
                URL = getResources().getString(R.string.url_for_check_out).toString();
//                System.out.println(currentUserId+" ---- "+currentDateCheckIn+" ---- "+currentDateCheckOut+" ---- "+ String.format("%.2f",pointage));
                attemptLogin.execute(currentUserId, startedDateTime, currentDateCheckOut, String.format("%.2f", pointage));

                editor.putBoolean("everCheckIn", false);
                editor.remove("CheckInTime");
                editor.commit();

                Toast.makeText(getApplicationContext(), "Worked : " + String.format("%.2f", pointage) + " Hours", Toast.LENGTH_SHORT).show();
            }
        });

        buttonSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(InTimeActivity.this,
                        SettingActivity.class);
                intent.putExtra("currentUserId", currentUserId);
                startActivity(intent);
                finish();
            }
        });

        //OLD FUNCTION

//        btnLogout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                SignOut();
//                Toast.makeText(getApplicationContext(), "Sign Out", Toast.LENGTH_SHORT).show();
//
//            }
//
//        });
    }

    private void SignOut() {
        Intent intent = new Intent(InTimeActivity.this,
                MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void initsInstance() {
        btnLogout = (Button) findViewById(R.id.btnLogout);
        name = (TextView) findViewById(R.id.name);
        localLogin = getIntent().getBooleanExtra("google", localLogin);
        checkInTime = (TextView) findViewById(R.id.checkInTime);
        checkOutTime = (TextView) findViewById(R.id.checkOutTime);
        differentTime = (TextView) findViewById(R.id.differentTime);
        buttonCheckIn = (Button) findViewById(R.id.buttonCheckIn);
        buttonCheckOut = (Button) findViewById(R.id.buttonCheckOut);
        buttonSetting = (Button) findViewById(R.id.buttonSetting);
        inTimeUsername = (TextView) findViewById(R.id.inTimeUsername);
        bar = (ProgressBar) findViewById(R.id.progressBarInTime);
        bar.getIndeterminateDrawable().setColorFilter(
                getResources().getColor(R.color.black),
                android.graphics.PorterDuff.Mode.SRC_IN);
        jsonParser = new JSONParser();

        currentUserId = getIntent().getStringExtra("currentUser");
        currentUserName = getIntent().getStringExtra("Username");
        currentDateCheckIn = "0000-00-00 00:00:00";
        currentDateCheckOut = "0000-00-00 00:00:00";
        pointage = 0.00;

//        pref.setUserName(, currentUserId);
        pref = getApplicationContext().getSharedPreferences("MyPref", 0);
        editor = pref.edit();
    }

    private void setCurrentTimeOut() {
        df = new android.text.format.DateFormat();
        currentTimeOutString = (String) df.format("HH:mm:ss", new Date());
        currentDateCheckOut = (String) df.format("yyyy-MM-dd HH:mm:ss", new Date());
        checkOutTime.setText(currentTimeOutString);

    }

    private void setCurrentTimeIn() {
        df = new android.text.format.DateFormat();
        currentTimeInString = (String) df.format("HH:mm:ss", new Date());
        currentDateCheckIn = (String) df.format("yyyy-MM-dd HH:mm:ss", new Date());
        checkInTime.setText(currentTimeInString);

    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
    }

    class AttemptLogin extends AsyncTask {

        @Override
        protected Object doInBackground(Object[] params) {
            String user_name = (String) params[0];
            String checkInTime = (String) params[1];
            String checkOutTime = (String) params[2];
            String pointageTime = (String) params[3];

            ArrayList params2 = new ArrayList();
//            params2.add(new BasicNameValuePair("user id", userId));
            params2.add(new BasicNameValuePair("user_name", user_name));
            params2.add(new BasicNameValuePair("check_in_time", checkInTime));
            params2.add(new BasicNameValuePair("check_out_time", checkOutTime));
            params2.add(new BasicNameValuePair("pointage", pointageTime));
//            System.out.println(params2.toString());
            JSONObject json = jsonParser.makeHttpRequest(URL, "POST", params2);


            return json;
        }

        protected void onPostExecute(Object result) {

            // dismiss the dialog once product deleted
            //Toast.makeText(getApplicationContext(),result,Toast.LENGTH_LONG).show();
            JSONObject JSONresult = (JSONObject) result;
            try {
                if (JSONresult != null) {
                    if (JSONresult.getInt("success") == 1) {

                    } else
                        Toast.makeText(getApplicationContext(), JSONresult.getString("message"), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Unable to retrieve any data from server", Toast.LENGTH_SHORT).show();
                }
//                bar.setVisibility(View.INVISIBLE);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        protected void onPreExecute() {
//            super.onPreExecute();

        }

    }

//    @Override
//    public void onBackPressed() {
//        if (doubleBackToExitPressedOnce) {
//            super.onBackPressed();
//            return;
//        }
//
//        this.doubleBackToExitPressedOnce = true;
//        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
//
//        new Handler().postDelayed(new Runnable() {
//
//            @Override
//            public void run() {
//                doubleBackToExitPressedOnce=false;
//            }
//        }, 2000);
//    }
}
